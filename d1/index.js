// Notes:
// The document refers to the whole page
// To access specific object models from the document, we can use:
// document.querySelector('#txt-first-name')
	// => query selector is much better as it is more effecient


// document.getElementById('txt-first-name')
// document.getElementsByClassName('txt-inputs')
// document.getElementByTagName('input')

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

// txtFirstName.addEventListener('keyup', (event) => {
// 	spanFullName.innerHTML = txtFirstName.value;
// });

// Multiple listeners can also be assigned to the same event
// txtFirstName.addEventListener('keyup', () => {
// 	console.log(event.target);
// 	console.log(event.target.value); // similar to the txtFirstName.value
// });

// Mini Activity
	// Listen to an event when the last name's input is changed
	// either add another event listener or to create a function that will update the span-full-name content

// my solution
// txtLastName.addEventListener('keyup', (event) => {
// 	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value
// });

// function - other solution for mini-activity
const updateFullName = () => {
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`;
};

txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);